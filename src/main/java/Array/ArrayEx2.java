package Array;

import java.util.Arrays;

public class ArrayEx2 {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 4);
        Cat murzik = new Cat("murzik", 3);

        Cat[] catsArray = new Cat[2];
        catsArray[0] = barsik;
        catsArray[1] = murzik;

        Cat[] cats = new Cat[]{barsik, murzik};

        System.out.println(Arrays.toString(catsArray));
    }
}
