package my.l1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
/*
Класс ConsoleReader
*/

public class ConsoleReader {
    public static String readString() throws Exception {
        Scanner scanner = new Scanner(System.in);
        String read = scanner.nextLine();
        return read;
    }

    public static int readInt() throws Exception {
        Scanner scanner = new Scanner(System.in);
        int read = scanner.nextInt();
        return read;
    }

    public static double readDouble() throws Exception {
        Scanner scanner = new Scanner(System.in);
        double read = scanner.nextDouble();
        return read;

    }

    public static boolean readBoolean() throws Exception {
        Scanner scanner = new Scanner(System.in);
        boolean read = scanner.nextBoolean();
        return read;

    }

    public static void main(String[] args) throws Exception {

    }
}































