package my.city;

public class Spider extends Arthropod {
    public static void main(String[] args) {
        Spider spider = new Spider();
        spider.printName(4);
        spider.printName(9.0);
    }

    public void printName(int input) {
        System.out.print("Spider");
    }
}