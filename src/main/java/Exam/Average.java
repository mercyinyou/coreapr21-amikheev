package Exam;

import java.util.Arrays;

public class Average {

        public static double average(int[] array) {
            return Arrays.stream(array).average().orElse(Double.NaN);
        }

    public static void main(String[] args) {
        int [] ar = {9,9};
        System.out.println(average(ar));
    }

    }

