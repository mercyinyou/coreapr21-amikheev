package Exam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Array23 {
    public static String arrayToString(char[] chars) {
        Map<Character, Integer> map = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        List<Character> listC = new ArrayList<Character>();
        for (char c : chars) {
            listC.add(c);
        }

        for (Character s : listC) {
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + 1);
            } else {
                map.put(s, 1);
            }
        }

        ArrayList<Map.Entry<Character, Integer>> listSorted = new ArrayList<>(map.entrySet());

        for (Map.Entry<Character, Integer> l : listSorted) {
            sb.append(l.getKey()).append(l.getValue());
        }
        return String.valueOf(sb);
    }

    public static void main(String[] args) {
        char[] chars = new char[]{'A', 'A', 'A', 'B', 'B', 'C', 'C', 'C'};
        System.out.println(arrayToString(chars));
    }
}
