package Exam;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class Time {
    public static String getTimeAfterMidnight(long seconds) {
        long millis = TimeUnit.SECONDS.toMillis(seconds);
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        Date date = new Date(millis);
        return (dateFormat.format(date));
    }

    public static void main(String[] args) {
         String time = getTimeAfterMidnight(600);
        System.out.println(time);
    }

}
