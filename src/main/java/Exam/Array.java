package Exam;

public class Array {
    //    public static void main(String[] args) {
//        char[] chars = new char[]{'A','A','A','B','B','C','C','C'};
//        Arrays.sort(chars);
//       for (char x:  chars ) {
//           System.out.print(x);
//       }
    public static String ArrayIntoString(char[] chars) {
        StringBuilder sb = new StringBuilder();
        for (char element : chars) {
            sb.append(element);
        }
        return String.valueOf(sb);
    }

    public static void main(String[] args) {
        char[] chars = new char[]{'A','A','A','B','B','C','C','C'};
        System.out.println(ArrayIntoString(chars));
    }
}

