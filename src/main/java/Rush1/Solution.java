package Rush1;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) throws InterruptedException {
        Map<String, String> map = new HashMap<>();

        map.put("арбуз", "ягода");
        map.put("банан", "трава");
        map.put("вишня", "ягода");
        map.put("груша", "фрукт");
        map.put("дыня", "овощь");
        map.put("ежевика", "куст");
        map.put("жень-шень", "корень");
        map.put("земляника", "ягода");
        map.put("ирис", "цветок");
        map.put("картофель", "клубень");



        ArrayList<Map.Entry<String, String>> listSorted = new ArrayList<>(map.entrySet());
        Collections.sort(listSorted, (e1,e2) -> e1.getKey().compareTo(e2.getKey()) );

        for (Map.Entry<String , String> m : listSorted) {
            System.out.println(m.getKey() + " - " + m.getValue());
        }
    }
}