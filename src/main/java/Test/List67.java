package Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class List67 {
    public static void main(String[] args) {


        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> list2 = new CopyOnWriteArrayList<>(list1);
        Map<Integer, Integer> map3 = new ConcurrentHashMap<>();
        map3.put(1, 2);
        map3.put(3, 3);

//        for (Integer item : list1) list1.add(10); // f1
        for (Integer item : list2) list2.add(item); // f2
        for (Integer key : map3.keySet()) map3.remove(key); // f2
        System.out.println(list1.size() + " " + list2.size() + " " + map3.size());
        System.out.println();
        System.out.println(map3);
    }
}


