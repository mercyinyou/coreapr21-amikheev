package Test;

public class Calculator implements CalcService{
    private CalcService calcService;

    public Calculator(CalcService calcService) {
        this.calcService = calcService;
    }

    public double sum(double val1, double val2) {
        return calcService.sum(val1,val2);
    }
}
