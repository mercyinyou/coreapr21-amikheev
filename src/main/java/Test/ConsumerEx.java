package Test;

import java.util.function.Consumer;

public class ConsumerEx {
    public static void main(String[] args) {
        Consumer<Integer> consumer = System.out::println;
        consumer.accept(767689);
    }
}
