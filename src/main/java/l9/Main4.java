package l9;

public class Main4 {
    public static void main(String[] args) {
        try {
            new Main4().getFoog("Barsik");
            new Main4().getFoog("Murzik");
            new Main4().getFoog("Snegok");
        } catch (IllegalArgumentException e) {
            System.err.println("Access denied");
        } catch (Exception e) {
            System.err.println(" SGWTL");
        }
        System.out.println();
    }

    void getFoog(String name) {
        if (name.equals("Barsik")) {
            System.out.println("i am eating");
        } else if (name.equals("Murzik")) {
            System.out.println(" Murzik waining 8pm");
        } else {
            throw new IllegalArgumentException("Access to food restricted to:" + name);
        }
    }
}
