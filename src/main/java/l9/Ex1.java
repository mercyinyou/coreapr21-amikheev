package l9;

import java.io.IOException;

public class Ex1 {
    public static void main(String[] args) {
        System.out.println("hello");
//        throw  new RuntimeException("i am exception");
//        System.out.println("by");


        //throw
        //throws - перекладывает ответственность на кого-то еще
        //try
        //catch
        //finally
        try {
            throw new RuntimeException("i am exception");
        } catch (RuntimeException e) {
           e.printStackTrace();
        } finally {
            System.err.println("finally");
        }
        System.out.println("by!");
    }

    void getMoney() throws IOException {
        throw new IOException();
    }

    void getMoney2() throws RuntimeException {
        throw new RuntimeException();
    }

    void getInfo() {
        try {
            getMoney();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getMoney2();
    }
}