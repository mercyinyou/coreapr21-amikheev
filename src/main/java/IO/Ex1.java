package IO;

import java.io.*;



public class Ex1 {
    public static void main(String[] args) throws IOException {
        byte [] bytesToWrite = {1,2,3};
        byte [] bytesReaded = new byte[10];

        String fileName = "C:\\Prototypes\\test.txt";
        FileOutputStream outputStream = null;
        FileInputStream inputStream = null;

        outputStream = new FileOutputStream(fileName);
        System.out.println("File is opened for writing...");
        outputStream.write(bytesToWrite);
        System.out.println("Wrote " + bytesToWrite.length + " byte");
        outputStream.close();
        System.out.println("Finished out");

        inputStream = new FileInputStream(fileName);
        System.out.println("File is opened to reading...");
        int bytesAvailable = inputStream.available();
        System.out.println("Ready for reading " + bytesAvailable + " byte");
        int count = inputStream.read(bytesReaded, 0, bytesAvailable);
        System.out.println("Read " + count + " byte");
        inputStream.close();
        System.out.println("Finished in");
    }
}
