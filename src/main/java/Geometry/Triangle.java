package Geometry;

public class Triangle {

    public Point a;
    public Point b;
    public Point c;

    public Triangle(Point a, Point b, Point c) {
        if (a == null || b == null || c == null) throw new IllegalArgumentException();
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double area() {
        return Math.abs((a.getX() - c.getX()) * (b.getY() - c.getY()) - (a.getY() - c.getY()) * (b.getX() - c.getX())) / 2;
    }

    public static void main(String[] args) {
         double RequiredArea = new Triangle(new Point(0,0), new Point(3,0), new Point(0,4)).area();
        System.out.println(RequiredArea);
    }

}

class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;

    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}