package Figures;




class Triangle extends Figure {
    public Point a;
    public Point b;
    public Point c;

    public Triangle(Point a, Point b, Point c) {
        if (a == null || b == null || c == null) throw new IllegalArgumentException();

        this.a = a;
        this.b = b;
        this.c = c;

        if (area() == 0) throw new IllegalArgumentException();

//        if ((a.getX() == b.getX() && b.getX() == c.getX()) && (a.getY() == b.getY()
//                && b.getY() == c.getY())) throw new IllegalArgumentException("Degenerative");

//        if (a == null || b == null || c == null) throw new IllegalArgumentException();
//
//        if (area() == 0) throw new IllegalArgumentException();

//        double lengthAB = new Segment(new Point(a.getX(), a.getY()), new Point(b.getX(), b.getY())).length();
//        double lengthBC = new Segment(new Point(b.getX(), b.getY()), new Point(c.getX(), c.getY())).length();
//        double lengthAC = new Segment(new Point(a.getX(), a.getY()), new Point(c.getX(), c.getY())).length();
//
//        if ((lengthAB + lengthBC < lengthAC) || (lengthBC + lengthAC < lengthAB) || (lengthAC + lengthAB < lengthBC))
//            throw new IllegalArgumentException();
    }

    public double area() {
        double lengthAB = new Segment(new Point(a.getX(), a.getY()), new Point(b.getX(), b.getY())).length();
        double lengthBC = new Segment(new Point(b.getX(), b.getY()), new Point(c.getX(), c.getY())).length();
        double lengthAC = new Segment(new Point(a.getX(), a.getY()), new Point(c.getX(), c.getY())).length();
        double s = (lengthAB + lengthBC + lengthAC) / 2;
        double area = Math.sqrt(s * (s - lengthAB) * (s - lengthBC) * (s - lengthAC));
        return area;
    }

    @Override
    public Point centroid() {
        return new Point((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
    }

    @Override
    public boolean isTheSame(Figure figure) {
        return false;
    }
}
