package Figures;

class Circle extends Figure {
    public Point center;
    public double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
        if (radius == 0 || center == null) throw new IllegalArgumentException();
        if (Math.abs(radius) == -radius) throw new IllegalArgumentException();
    }

    @Override
    public Point centroid() {
        return new Point(center.getX(), center.getY());
    }

    @Override
    public boolean isTheSame(Figure figure) {
        return false;
    }
}
