package Figures;


class Segment {
    public Point start;
    public Point end;

    public Segment(Point start, Point end) {
//        if (start == null || end == null || (start.getX() == end.getX() && start.getY() == end.getY()))
//            throw new RuntimeException();
        this.start = start;
        this.end = end;
    }

    double length() {
        double length = Math.sqrt(Math.pow(end.getX() - start.getX(), 2) + Math.pow(end.getY() - start.getY(), 2));
        return length;
    }

    Point middle() {
        return new Point((start.getX() + end.getX()) / 2, (start.getY() + end.getY()) / 2);
    }

    Point intersection(Segment another) {
        double a1 = end.getY() - start.getY();
        double b1 = start.getX() - end.getX();
        double c1 = a1 * start.getX() + b1 * start.getY();

        double a2 = another.end.getY() - another.start.getY();
        double b2 = another.start.getX() - another.end.getX();
        double c2 = a2 * another.start.getX() + b2 * another.start.getY();

        double delta = a1 * b2 - a2 * b1;
        double Px = (b2 * c1 - b1 * c2) / delta;
        double Py = (a1 * c2 - a2 * c1) / delta;

        boolean a = Math.min(start.getX(), end.getX()) <= Px && Px <= Math.max(start.getX(), end.getX());
        boolean b = Math.min(another.start.getX(), another.end.getX()) <= Px && Px <= Math.max(another.start.getX(), another.end.getX());
        if (delta == 0 || a == false || b == false) {
            return null;
        } else {
            return new Point(Px, Py);
        }
    }
}