package Figures;

class Quadrilateral extends Figure {
    public Point a;
    public Point b;
    public Point c;
    public Point d;
    double area1;
    double area2;
    double area3;
    double area4;
//    Point p;


    public Quadrilateral(Point a, Point b, Point c, Point d) {
        if (a == null || b == null || c == null || d == null) throw new IllegalArgumentException();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        double diagAC = new Segment(new Point(a.getX(), a.getY()), new Point(c.getX(), c.getY())).length(); //Диагональ AC
        double diagBD = new Segment(new Point(b.getX(), b.getY()), new Point(d.getX(), d.getY())).length();
        double lengthAB = new Segment(new Point(a.getX(), a.getY()), new Point(b.getX(), b.getY())).length();
        double lengthBC = new Segment(new Point(b.getX(), b.getY()), new Point(c.getX(), c.getY())).length();
        double lengthCD = new Segment(new Point(c.getX(), c.getY()), new Point(d.getX(), d.getY())).length();
        double lengthDA = new Segment(new Point(d.getX(), d.getY()), new Point(a.getX(), a.getY())).length();
        area1 = new Triangle(new Point(a.getX(), a.getY()), new Point(b.getX(), b.getY()), new Point(c.getX(), c.getY())).area();
        area2 = new Triangle(new Point(a.getX(), a.getY()), new Point(c.getX(), c.getY()), new Point(d.getX(), d.getY())).area();
        area1 = new Triangle(new Point(a.getX(), a.getY()), new Point(b.getX(), b.getY()), new Point(d.getX(), d.getY())).area();
        area1 = new Triangle(new Point(b.getX(), b.getY()), new Point(c.getX(), c.getY()), new Point(d.getX(), d.getY())).area();

//        if (area() == 0) throw new IllegalArgumentException();
        if (diagAC < lengthAB && diagAC < lengthBC || diagBD < lengthBC && diagBD < lengthCD) throw new IllegalArgumentException();
        if (area1 < 0 || area2 <0 || area3 <0 || area4 < 0)  throw new IllegalArgumentException();






//        if (diagAC < lengthAB || diagAC < lengthBC || diagAC < lengthCD || diagAC < lengthDA ||
//                diagBD < lengthAB || diagBD < lengthBC || diagBD < lengthCD || diagBD < lengthDA) throw new IllegalArgumentException();





//        Segment lengthAC = new Segment(new Point(a.getX(), a.getY()), new Point(c.getX(), c.getY())); //Диагональ AC
//        Segment lengthBD = new Segment(new Point(b.getX(), b.getY()), new Point(d.getX(), d.getY()));

//        Point intersection = lengthAC.intersection(lengthBD);
//        p = new Point(intersection.getX(), intersection.getY());




//


//
//
//
//
//        if ((lengthAB == lengthBC + lengthCD + lengthDA) && (lengthBC == lengthAB + lengthCD + lengthDA) &&
//                (lengthCD == lengthBC + lengthAB + lengthDA) && (lengthDA == lengthBC + lengthCD + lengthAB))
//            throw new IllegalArgumentException();
//        if (lengthAB == 0 || lengthBC == 0 || lengthCD == 0 || lengthDA == 0) new IllegalArgumentException();
//
    }

    public double area() {
        double area = (a.getX() - b.getX()) * (a.getY() + b.getY()) + (b.getX() - c.getX()) * (b.getY() + c.getY()) +
                (c.getX() - d.getX()) * (c.getY() + d.getY()) + (d.getX() - a.getX()) * (d.getY() + a.getY());
        return Math.abs(area / 2);
    }

    @Override
    public Point centroid() {

//        Segment lengthAC = new Segment(new Point(a.getX(), a.getY()), new Point(c.getX(), c.getY()));
//        Segment lengthBD = new Segment(new Point(b.getX(), b.getY()), new Point(d.getX(), d.getY()));
//        Point intersection = lengthAC.intersection(lengthBD);
        return null;
    }

    @Override
    public boolean isTheSame(Figure figure) {
        return false;
    }
}




