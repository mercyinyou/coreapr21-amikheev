package Time;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class TimeWatch {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        long seconds = scanner.nextLong();
        long milliseconds = TimeUnit.SECONDS.toMillis(seconds);

        SimpleDateFormat formater = new SimpleDateFormat("H:mm:ss");
        Date date = new Date(milliseconds);
        System.out.println(formater.format(date));
    }


    }
