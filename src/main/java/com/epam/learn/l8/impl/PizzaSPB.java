package com.epam.learn.l8.impl;

import com.epam.learn.l8.PizzaInterface;

public class PizzaSPB implements PizzaInterface {
    @Override
    public void wash() {
        System.out.println("i am washing hands");
    }

    @Override
    public void cook() {
        System.out.println("i am cooking with fishhhhmeatbeat");
    }

    @Override
    public void delivery() {
        System.out.println("sending to client");

    }

    @Override
    public void pay() {
        System.out.println("free pizza");
    }

    @Override
    public void check() {

    }
}
