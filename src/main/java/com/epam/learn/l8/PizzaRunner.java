package com.epam.learn.l8;

import com.epam.learn.l8.impl.PizzaMSK;
import com.epam.learn.l8.impl.PizzaSPB;

public class PizzaRunner {
    private PizzaInterface pizzaMsk;
    private PizzaInterface pizza;
    private PizzaInterface pizzaSpb;

    public static void main(String[] args) {
        PizzaRunner runner = new PizzaRunner();
        runner.getPizzaInfo();
    }

    private void  getPizzaInfo() {
        if (true) {
            pizza = new PizzaMSK();
        } else {
            pizza  = new PizzaSPB();
        }
        pizzaMsk = new PizzaMSK();
        pizzaSpb = new PizzaSPB();
        pizzaMsk.wash();
        pizzaMsk.sayHello();
        pizzaSpb.wash();
        pizzaMsk.cook();
        pizzaSpb.cook();
        pizzaMsk.delivery();
        pizzaSpb.delivery();
    }
}
