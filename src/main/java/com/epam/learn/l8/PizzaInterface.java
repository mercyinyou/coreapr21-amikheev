package com.epam.learn.l8;

public interface PizzaInterface extends Payable, Cheker {
   public static void sayHi(){
       System.out.println("Static hello from interface");
   }

    default  void sayHello() {
        System.out.println("Hello from java");
    }

    void wash();
    void cook();
    void delivery();
}
