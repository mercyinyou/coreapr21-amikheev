package com.epam.learn.l5;

public class Main2 {
    public static void main(String[] args) {
        String name = "Barsik";

        System.out.println(name.charAt(2));
        System.out.println(name.length());
        System.out.println(name.subSequence(2, 5));

        char[] chars = new char[5];
        name.getChars(2, 4, chars, 2);

        System.out.println(chars);
    }
}
