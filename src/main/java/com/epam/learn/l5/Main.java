package com.epam.learn.l5;

import java.io.UnsupportedEncodingException;

public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //immutable
        String s = "this is a string";
        String s1 = "";
        String s2 = null;
        s = "rgdrdrgdrgd";
        char[] charArray = s.toCharArray();
        String s3 = new String(charArray, 2, 3);
        String s4 = new String(charArray);
        System.out.println(s3);

        byte[] ascii = {65, 66, 67, 68, 69, 70};
        System.out.println(new String(ascii));

        byte[] data = {(byte) 0xE3, (byte) 0xEE};
        System.out.println(new String(data, "CP866"));
    }
}

