package com.javarush.task.task05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task0507 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String linne = reader.readLine();
        int count = 1;

        while (true) {
            count++;
            int number = Integer.parseInt(linne);
            if (number == -1) {
                break;
            }
            number = number + number;
            System.out.println(number / (count));
        }
    }
}
