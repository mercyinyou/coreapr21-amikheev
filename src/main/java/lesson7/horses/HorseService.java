package lesson7.horses;

public class HorseService {
    public static void main(String[] args) {

        Horse risak = new Horse(20,"risak", true, true);
        Horse sizay = new Horse(30,"sizay", true, false);

        printSpeed(risak, O -> O.getSpeed());
        printName(risak, O -> O.getName());

        printSpeed(sizay, U -> U.getSpeed());
        printName(sizay, HOR -> HOR.getName());
    }

    private static void printName (Horse horse, GetName getName) {
        System.out.println(getName.get(horse));
    }

    private static void printSpeed(Horse horse, GetSpeed getSpeed) {
        System.out.println(getSpeed.get(horse));
    }


}
