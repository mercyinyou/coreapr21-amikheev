package lesson7.generic;

public class HorseService {
    public static void main(String[] args) {

        Horse bura = new Horse(20, "Bur", true, true);
        Horse siz = new Horse(500, "Siz", true, false);

        print(bura, O -> O.getSpeed());
        print(bura, O -> O.getName());
        print(siz, O -> O.getSpeed());
        print(siz, O -> O.getName());
    }

    private static void print(Horse horse, Get get) {
        System.out.println(get.get(horse));
    }
}
