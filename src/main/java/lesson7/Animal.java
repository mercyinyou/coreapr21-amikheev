package lesson7;


@FunctionalInterface
public interface Animal {
    String getInfo(String name);
}
