package lesson7;

import javax.xml.namespace.QName;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        Animal cat = new Cat();
        System.out.println(cat.getInfo("barsik"));

        Animal animal = new Animal() {
            @Override
            public String getInfo(String name) {
                return "i am animal with name: " + name;
            }
        };

        Animal animal2 = name -> "i am animal with name: " + name;

        System.out.println(animal.getInfo("rab"));
        System.out.println(animal2.getInfo("rokkr"));
        FuncEx1 funcEx1 = new RealFunc();
        funcEx1.getInfo("box");

        FuncEx1 funcEx2 = klikyxa -> {
            klikyxa = klikyxa.toUpperCase();
            System.out.println("I am abstract lambda " + klikyxa);
        };

        funcEx2.getInfo("lox");
    }
}
