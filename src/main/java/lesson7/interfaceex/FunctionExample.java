package lesson7.interfaceex;

import lesson7.FunctionalInterfaceExample;

import java.util.Locale;
import java.util.function.Function;
import java.util.function.BiFunction;

public class FunctionExample {
    public static void main(String[] args) {

        Function<String, String> function = s -> s.toUpperCase();
        System.out.println(function.apply("Functional interface java 8"));

        Function<String, Integer> function2 = s -> s.length();
        System.out.println(function2.apply("Fuckf,ekm"));

        BiFunction<String,String,String> biFunction = (s, s2) -> s.concat(s2);
        System.out.println(biFunction.apply("Xep ", "Vam"));
    }
}
