package lesson7;

public class Cat implements Animal{

    @Override
    public String getInfo(String name) {
        return "i am cat with name: " + name;
    }
}
