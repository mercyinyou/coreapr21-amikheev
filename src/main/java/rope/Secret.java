package rope;

interface Secret {
    String magic(double d);
}
